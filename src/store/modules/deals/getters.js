export const coord = (state) => state.map((item) => {
  return {position: {lat: +item.coord.slice(',', 2), lng: +item.coord.slice(-2)}}
})
